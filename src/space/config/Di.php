<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\space\space\model\SpaceEntityFactory;
use people_sdk\space\space\model\repository\SpaceEntitySimpleRepository;



return array(
    // Space entity services
    // ******************************************************************************

    'people_space_entity_factory' => [
        'source' => SpaceEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_space_entity_simple_repository' => [
        'source' => SpaceEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);